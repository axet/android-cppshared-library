# c++_shared

Android NDK comes with libc++_shared but here is no propper way to include libc++_shared.so into apk unless you complie native library using gradle. For projects which uses prebuild libraries you can use this dependecy. If you build from source, you can safely replace projects depend on 'libcppshared' with library from android NDK.

```gradle
dependencies {
    compile 'com.github.axet:libcppshared:n17'
}
```